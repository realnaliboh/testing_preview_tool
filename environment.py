from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import testingbotclient


API_KEY = "8139eb0370ed8eb6c97d8ac1161ccf1a"
API_SECRET = "e780b2c946a01a79e0b06d7b80684e59"

browsers = [
  { "platform":"WINDOWS", "browserName" : "firefox", "version" : "latest"},
  { "platform":"WINDOWS", "browserName" : "firefox", "version" : "latest"},
  { "platform":"LINUX", "browserName" : "chrome", "version" : "latest"},
  { "platform":"MAC", "browserName" : "safari", "version" : "latest"}
]


def before_all(context):
    context.browser = webdriver.Remote(
        command_executor="http://%s:%s@hub.testingbot.com:4444/wd/hub" % (API_KEY, API_SECRET),
        desired_capabilities={ "platform":"WIN10", "browserName": "chrome", "version": "latest"})
    tb = testingbotclient.TestingBotClient('%s' % API_KEY, '%s' % API_SECRET)
    tb.tests.update_test(context.browser.session_id, status_message='', passed=1 | 0,
                         name='preview_test')


def after_all(context):
    context.browser.quit()







