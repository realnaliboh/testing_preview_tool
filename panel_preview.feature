# Created by realnaliboh at 02.12.2020
Feature: preview tool

  Background:
    Given I am logged in

  Scenario: use preview tool for a given company and site
    Given I go to the preview tool page
    When I choose a company and a site and click go
    And provide a picture to search for recommendations
    And look for recommendations
    Then I can see the recommendations for this picture