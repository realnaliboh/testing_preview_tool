from behave import *

use_step_matcher("re")


@given("I am on the panel start page")
def step_impl(context):
    context.browser.get("https://panel.mediaopt.tools/login")


@when("enter my credentials and press submit")
def step_impl(context):
    username = context.browser.find_element_by_name("_username")
    username.send_keys("valeria.blokhina@mediaopt.de")
    password = context.browser.find_element_by_name("_password")
    password.send_keys("191325Asdgroopuu")
    context.browser.find_element_by_name("_submit").click()

@then("I am logged in")
def step_impl(context):
    assert "Dashboard" in context.browser.title


@given("I am logged in")
def step_impl(context):
    context.browser.get("https://panel.mediaopt.tools/login")
    username = context.browser.find_element_by_name("_username")
    username.send_keys("valeria.blokhina@mediaopt.de")
    password = context.browser.find_element_by_name("_password")
    password.send_keys("191325Asdgroopuu")
    context.browser.find_element_by_name("_submit").click()
    assert "Dashboard" in context.browser.title