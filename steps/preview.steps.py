from behave import *

use_step_matcher("re")


@given("I go to the preview tool page")
def step_impl(context):
    context.browser.find_element_by_id('navigation-preview_tool').click()


@when("I choose a company and a site and click go")
def step_impl(context):
    def expand_shadow_element(element):
        shadow_root = context.browser.execute_script('return arguments[0].shadowRoot', element)
        return shadow_root

    context.browser.find_element_by_id('navigation-preview_tool').click()
    root1 = context.browser.find_element_by_css_selector('mopt-site-selector')
    shadow_root1 = expand_shadow_element(root1)
    root2 = shadow_root1.find_element_by_css_selector('mopt-company-dropdown')
    shadow_root2 = expand_shadow_element(root2)
    shadow_root2.find_element_by_css_selector('select > option:nth-child(8)').click()
    root3 = shadow_root1.find_element_by_css_selector('mopt-site-dropdown')
    shadow_root3 = expand_shadow_element(root3)
    shadow_root3.find_element_by_css_selector('select > option:nth-child(3)').click()
    shadow_root1.find_element_by_css_selector('mopt-button').click()


@step("provide a picture to search for recommendations")
def step_impl(context):
    def expand_shadow_element(element):
        shadow_root = context.browser.execute_script('return arguments[0].shadowRoot', element)
        return shadow_root

    root4 = context.browser.find_element_by_css_selector('#preview-tool > mopt-preview-tool')
    shadow_root4 = expand_shadow_element(root4)
    root5 = shadow_root4.find_element_by_css_selector('mopt-step.source.hydrated > mopt-visual-source-selector')
    shadow_root5 = expand_shadow_element(root5)
    picture = shadow_root5.find_element_by_css_selector('form > input[type=url]')
    picture.send_keys(
        'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fonline-moebel-kaufen.de%2Fshop%2Fimages%2Fproduct_images%2Fpopup_images%2F22956_0.jpg&f=1&nofb=1')
    shadow_root5.find_element_by_css_selector('form > button').click()


@step("look for recommendations")
def step_impl(context):
    def expand_shadow_element(element):
        shadow_root = context.browser.execute_script('return arguments[0].shadowRoot', element)
        return shadow_root

    root4 = context.browser.find_element_by_css_selector('#preview-tool > mopt-preview-tool')
    shadow_root4 = expand_shadow_element(root4)
    shadow_root4.find_element_by_css_selector(
        'mopt-step.object-selector.hydrated > mopt-objects-gallery > mopt-coco-image').click()


@then("I can see the recommendations for this picture")
def step_impl(context):
    def expand_shadow_element(element):
        shadow_root = context.browser.execute_script('return arguments[0].shadowRoot', element)
        return shadow_root

    root4 = context.browser.find_element_by_css_selector('#preview-tool > mopt-preview-tool')
    shadow_root4 = expand_shadow_element(root4)
    root6 = shadow_root4.find_element_by_css_selector(
        'mopt-step.search-results.hydrated > div > mopt-preview-recommendation:nth-child(1)')
    shadow_root6 = expand_shadow_element(root6)
    shadow_root6.find_element_by_css_selector('mopt-button').click()

